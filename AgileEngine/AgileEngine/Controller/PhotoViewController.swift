//
//  PhotoViewController.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/20/19..
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import UIKit


class PhotoViewController : UIPageViewController{
    
    var photos = ["photo1","photo2","photo3","photo4","photo5","photo1","photo2","photo3","photo4","photo5","photo1","photo2","photo3","photo4","photo5"]
    var currentIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //dataSource = self as? UIPageViewControllerDataSource
        self.dataSource = self
        
        if let viewController = self.viewPhotoCommentController(0) {
            self.setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
        }
        
    }
    
    func viewPhotoCommentController(_ index: Int) -> FullPhotoViewController? {
        if let storyboard = storyboard,
            let page = storyboard.instantiateViewController(withIdentifier: "PhotoViewController") as? FullPhotoViewController {
            page.photoName = photos[index]
            page.photoIndex = index
            return page
        }
        return nil
    }
}

//MARK: implementation of UIPageViewControllerDataSource

extension PhotoViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if let viewController = viewController as? FullPhotoViewController,
            let index = viewController.photoIndex,
            index > 0 {
            return viewPhotoCommentController(index - 1)
        }

        return nil
    }

    // 2
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if let viewController = viewController as? FullPhotoViewController,
            let index = viewController.photoIndex,
            (index + 1) < photos.count {
            return viewPhotoCommentController(index + 1)
        }

        return nil
    }

    // MARK: UIPageControl
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return photos.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex ?? 0
    }
}
