//
//  Models.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/24/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation



class ImageModel : Codable {
    let id : String
    let previewURL : String
    var detail : ImageDetailModel?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case previewURL
        case detail
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        previewURL = try container.decode(String.self, forKey: .previewURL)
        detail = try container.decode(ImageDetailModel.self, forKey: .detail)
    }
}

class ImageDetailModel : Codable {
    let author : String
    let fullPhotoURL : String
    
    enum CodingKeys: String, CodingKey {
        case author
        case fullPhotoURL
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        author = try container.decode(String.self, forKey: .author)
        fullPhotoURL = try container.decode(String.self, forKey: .fullPhotoURL)
    }
}

class ImagesPageModel : Codable {
    var imageModel : [ImageModel] = []
    var imageCurrenPage : Int
    var imagesCountOfPage : Int
    
    enum CodingKeys: String, CodingKey {
        case imageModel
        case imageCurrenPage
        case imagesCountOfPage
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageModel = try container.decode([ImageModel].self, forKey: .imageModel)
        imageCurrenPage = try container.decode(Int.self, forKey: .imageCurrenPage)
        imagesCountOfPage = try container.decode(Int.self, forKey: .imagesCountOfPage)
    }
}

struct TokenModel : Decodable {
    let tokenModel : String
}
