//
//  Request.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/24/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation

class Request {
    
    typealias complitionHandlerToken         = (_ token      : String?)      -> ()
    typealias complitionHablerID             = (_ id         : String)       -> ()
    typealias complitionHablerPageNumber     = (_ pageNumber : Int)          -> ()
    typealias complitionHablerImages         = (_ images     : [ImageModel]) -> ()
    
    func getRequest(complition : @escaping complitionHandlerToken){
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache"
        ]
        let parameters = ["apiKey": "23567b218376f79d9415"] as [String : Any]
        
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://195.39.233.28:8035/auth")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
            }
            guard let data = data else {return}
            let token = try! JSONDecoder().decode(TokenModel.self, from: data)
            print(token as Any)
            complition(token.tokenModel)
            //            guard let tokenForTime = timeToken?.token else {return}
            //            print(tokenForTime)
        })
        dataTask.resume()
    }
    // MARK: Method for get images pages
    func postRequestImages(token : String, complition : @escaping complitionHablerPageNumber){
        let headers = [
            "Authorization": "Bearer \(token)",
            "cache-control": "no-cache"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://195.39.233.28:8035/images")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
            }
            guard let data = data else {return}
            print(data)
            do{
                let  dataInfo = try JSONDecoder().decode(ImagesPageModel.self, from: data)
                print(dataInfo as Any)
                complition(dataInfo.imagesCountOfPage)
            } catch{
                print("Somsing with Decoder",error)
            }
        })
        dataTask.resume()
    }
    
    // MARK: get all Id
    func postRequestId(token : String, ImagesPageModel : ImagesPageModel, complition : @escaping complitionHablerID){
        let headers = [
            "Authorization": "Bearer \(token)",
            "cache-control": "no-cache"
        ]
        let request = NSMutableURLRequest(url: NSURL(string: "http://195.39.233.28:8035/images?page=\(ImagesPageModel.imageCurrenPage)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
            }
            guard let data = data else {return}
            print(data)
            do{
                let  dataInfo = try JSONDecoder().decode(ImageModel.self, from: data)
                print(dataInfo as Any)
                complition(dataInfo.id)
                
            } catch{
                print("Somsing with Decoder",error)
            }
        })
        dataTask.resume()
    }
    
    // MARK: Method for get all image info and big size of it
    func postRequestDetail(token : String , complition : @escaping complitionHablerImages , imageModel : ImageModel , pageNumber : ImagesPageModel){
        let headers = [
            "Authorization": "Bearer \(token)",
            "cache-control": "no-cache"
        ]
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://195.39.233.28:8035/images?page=\(pageNumber.imageCurrenPage)/\(imageModel.id)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
            }
            guard let data = data else {return}
            print(data)
            do{
                let  dataInfo = try JSONDecoder().decode(ImageModel.self, from: data)
                print(dataInfo as Any)
                complition([dataInfo])
                
            } catch{
                print("Somsing with Decoder",error)
            }
        })
        dataTask.resume()
    }
}

/*
 Screen 1
    login запрос auth
    ImageS - images/page1..n
 
    UICollectView
 
 Screen 3
    deteilImage images/id
 
    UIImageView + Lables
 
 */

