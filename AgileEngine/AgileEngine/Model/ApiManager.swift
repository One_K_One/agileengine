//
//  ApiManager.swift
//  AgileEngine
//
//  Created by Константин Овчаренко on 3/24/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation


class ApiManager {
    
    
   private init(){
        
    }
    
    static let shared = ApiManager()
    private var token : String?
    private let request = Request()
    private var arrayOfImageURL = [""]
    

    func auth(complition: @escaping () -> ()){
        self.request.getRequest { (token : String?) -> () in
            self.token = token
            complition()
        }
        
    }
    
    func getImages(page : Int , complition : @escaping ([ImageModel]) -> ()) {
        guard let token = self.token else {
            complition([])
            return
        }
    }
}
